//
//  MemoItem.h
//  MemoList
//
//  Created by Wuhua Dai on 10/18/14.
//  Copyright (c) 2014 Hazytint. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MemoItem : NSObject
@property NSString *itemName;
@property BOOL completed;
@property (readonly) NSDate *creationDate;
@end
