//
//  ViewController.m
//  MemoList
//
//  Created by Wuhua Dai on 10/18/14.
//  Copyright (c) 2014 Hazytint. All rights reserved.
//

#import "AddMemoItemViewController.h"

@interface AddMemoItemViewController ()
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButton;

@end

@implementation AddMemoItemViewController

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if (sender != self.doneButton) return;
    if (self.textField.text.length > 0) {
        self.memoitem = [[MemoItem alloc] init];
        self.memoitem.itemName = self.textField.text;
        self.memoitem.completed = NO;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
