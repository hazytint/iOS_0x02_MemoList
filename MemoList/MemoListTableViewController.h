//
//  MemoListTableViewController.h
//  MemoList
//
//  Created by Wuhua Dai on 10/18/14.
//  Copyright (c) 2014 Hazytint. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MemoListTableViewController : UITableViewController
-(IBAction)unwindToList:(UIStoryboardSegue*)segue;
@end
